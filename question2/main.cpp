#include "stdio.h"
#include "x86intrin.h"
#include <iostream>

using namespace std;

typedef union {
	__m128i 			int128;
	
	unsigned char		m128_u8[16];
	signed char			m128_i8[16];

	unsigned short		m128_u16[8];
	signed short		m128_i16[8];

	unsigned int		m128_u32[4];
	signed int			m128_i32[4];

	unsigned long		m128_u64[2];
	signed long			m128_i64[2];
} intVec;


void print_u8 (__m128i a)
{
	intVec tmp;
	tmp.int128 = a;
	printf ("[");
	for (int i=15; i>0; i--) {
		printf ("%X, ", tmp.m128_u8[i]);
	}
	printf ("%X]\n", tmp.m128_u8[0]);
}

void print_i8 (__m128i a)
{
	intVec tmp;
	tmp.int128 = a;
	printf ("[");
	for (int i=15; i>0; i--) {
		printf ("%X, ", tmp.m128_i8[i]);
	}
	printf ("%X]\n", tmp.m128_i8[0]);
}

void print_u16 (__m128i a)
{
	intVec tmp;
	tmp.int128 = a;
	cout << "[";
	for (int i=7; i>0; i--) {
		cout << tmp.m128_u16[i] << ", ";
	}
	cout << tmp.m128_u16[0] << "]" << endl;
}

void print_i16 (__m128i a)
{
	intVec tmp;
	tmp.int128 = a;
	cout << "[";
	for (int i=7; i>0; i--) {
		cout << tmp.m128_i16[i] << ", ";
	}
	cout << tmp.m128_i16[0] << "]" << endl;
}

void print_u32 (__m128i a)
{
	intVec tmp;
	tmp.int128 = a;
	cout << "[";
	for (int i=3; i>0; i--) {
		cout << tmp.m128_u32[i] << ", ";
	}
	cout << tmp.m128_u32[0] << "]" << endl;
}

void print_i32 (__m128i a)
{
	intVec tmp;
	tmp.int128 = a;
	cout << "[";
	for (int i=3; i>0; i--) {
		cout << tmp.m128_i32[i] << ", ";
	}
	cout << tmp.m128_i32[0] << "]" << endl;
}

void print_u64 (__m128i a)
{
	intVec tmp;
	tmp.int128 = a;
	cout << "[" << tmp.m128_u64[1] << ", " << tmp.m128_u64[0] << "]" << endl;

}

void print_i64 (__m128i a)
{
	intVec tmp;
	tmp.int128 = a;
	cout << "[" << tmp.m128_i64[1] << ", " << tmp.m128_i64[0] << "]" << endl;
}

void print_int_vector (__m128i a, string type)
{
	if (type == "u8")
		print_u8 (a);
	else if (type == "i8")
		print_i8 (a);
	else if (type == "u16")
		print_u16 (a);
	else if (type == "i16")
		print_i16 (a);
	else if (type == "u32")
		print_u32 (a);
	else if (type == "i32")
		print_i32 (a);
	else if (type == "u64")
		print_u64 (a);
	else if (type == "i64")
		print_i64 (a);
}

typedef union {
	__m128 			sp128;
	float			m128_fp[4];
} spfpVec;


void print_spfp_vector (__m128 a)
{
	spfpVec tmp;
	tmp.sp128 = a;
	printf ("[");
	for (int i=3; i>0; i--) {
		cout << tmp.m128_fp[i] << ", ";
	}
	cout << tmp.m128_fp[0] << "]" << endl;
}

// int main(void)
// {

// 	return 0;
// }
