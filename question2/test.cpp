#include "stdio.h"
#include "x86intrin.h"
#include <iostream>
#include "main.h"
using namespace std;

unsigned char intArray [16] = {	0X00, 0X11, 0X22, 0X33, 0X44, 0X55, 0X66, 0X77,
								0X88, 0X99, 0XAA, 0XBB, 0XCC, 0XDD, 0XEE, 0XFF};

// unsigned char intArray [16] = {	0b11111111, 0b11111111, 0b11111111, 0b11111111, 0b11111111, 0b11111111, 0b11111111, 0b11111111,
// 								0b11111111, 0b11111111, 0b11111111, 0b11111111, 0b11111111, 0b11111111, 0b11111111, 0b11111111};


float floatArray [4] = {1.1, 2.2, 3.3, 4.4};

int main(void)
{
	__m128i a;
	a = _mm_load_si128((const __m128i*)intArray);

	string types[8] = {"u8", "i8", "u16", "i16", "u32", "i32", "u64", "i64"};

	for (int i = 0; i < 8; i++) {
		cout << types[i] << ":\n";
		print_int_vector(a, types[i]);
		cout << endl;
	}

	return 0;
}
